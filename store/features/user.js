/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: null,
    token: null,
    score: null,
    loading: false,
    error: null,
  },
  reducers: {
    apiRequested: (state, action) => {
      state.loading = true;
    },
    apiRequestFailed: (state, action) => {
      state.loading = false;
      state.error = action.payload.error;
    },
    setCredentials: (state, action) => {
      const { user, accessToken } = action.payload;

      state.user = user;
      state.token = accessToken;
      state.loading = false;
    },
    logOut: (state, action) => {
      state.user = null;
      state.token = null;
    },
  },
});

export const selectedCurUser = (state) => state.user.user;
export const selectedCurToken = (state) => state.user.token;
export const selectedCurLoading = (state) => state.user.loading;
export const selectedCurScore = (state) => state.user.score;
export const userSliceAction = userSlice.actions;
export default userSlice.reducer;
