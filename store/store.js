import { configureStore } from '@reduxjs/toolkit';
import { userSlice } from './features/user';
import apiMiddleware from './middleware/api';

// config the store
const store = configureStore({
  reducer: {
    user: userSlice.reducer,
  },
  middleware: (getDefaultMiddleware) => [
    ...getDefaultMiddleware(),
    apiMiddleware,
  ],
});

export default store;
