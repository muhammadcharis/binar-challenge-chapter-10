// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC04GLiOSk5oSI57d5_70ltlXPu7PqwJ_w",
  authDomain: "chapter-9-ad91c.firebaseapp.com",
  projectId: "chapter-9-ad91c",
  storageBucket: "chapter-9-ad91c.appspot.com",
  messagingSenderId: "250869548820",
  appId: "1:250869548820:web:e66b4e617c109af72ea401",
  measurementId: "G-QSXJ6S53MN"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export const storage = getStorage(app)

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth();