import Head from 'next/head';
import { Navigation } from './Navigation';
import { Footer } from './Footer';

function Layout({ children }) {
  return (
    <>
      <Head>
        <title>CH-10</title>
      </Head>
      <Navigation />
      <main>{children}</main>
      <Footer />
    </>
  );
}

export default Layout;
