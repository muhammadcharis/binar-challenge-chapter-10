import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { signInWithPopup, GoogleAuthProvider } from 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../firebase';
import axios from './api/axios';
import { useDispatch } from 'react-redux';
import { userSliceAction } from '../store/features/user';
import Image from 'next/image';

function Index() {
  const onSubmitHandle = async (e) => {
    e.preventDefault();

    const inputLogin = {
      email,
      password,
    };

    await axios
      .post('/users/login', JSON.stringify(inputLogin), {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then((result) => {
        const data = result?.data;

        if (data.response === 200) {
          dispatch(
            userSliceAction.setCredentials({
              user: data.name,
              accessToken: data.accessToken,
            }),
          ).then(() => {
            router.replace('/');
            localStorage.setItem('key', data.accessToken);
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isShowPassword, setIsShowPassword] = useState('');
  const [user, setUser] = useAuthState(auth);
  const dispatch = useDispatch();
  const googleAuth = new GoogleAuthProvider();
  const googleLogin = async () => {
    const result = await signInWithPopup(auth, googleAuth);
  };

  useEffect(() => {
    if (user) {
      dispatch(
        userSliceAction.setCredentials({
          user: user.displayName,
          accessToken: user.accessToken,
        }),
      ).then(() => {
        router.replace('/');
      });
    }
  }, [user]);

  return (
    <section className="mx-auto h-screen grid place-content-center w-screen">
      <div className="container w-80 lg:w-screen">
        <div className="grid grid-rows-1 lg:grid-cols-2 lg:gap-1">
          <div className="rounded-2xl lg:block hidden">
            <Image
              src={'/images/login.jpg'}
              alt="player one"
              className="object-cover"
              width={500}
              height={500}
            />
          </div>
          <div className="lg:flex lg:justify-center">
            <div className="bg-white border border-gray-200 shadow-md p-6 rounded-lg ">
              <div className="flex flex-col gap-1 lg:justify-center lg:items-center h-full">
                <div className="text-start lg:w-96 ">
                  <h2 className="text-start text-lg lg:text-3xl font-bold tracking-tight text-gray-900">
                    Sign In
                  </h2>
                  <p className="mt-2 text-start text-sm text-gray-600">
                    Or
                    <Link
                      href="/register"
                      className="font-medium text-indigo-600 hover:text-indigo-500 ml-1 underline"
                    >
                      Create an account
                    </Link>
                  </p>
                </div>
                <form
                  onSubmit={(e) => onSubmitHandle(e)}
                  className="mt-8 space-y-6 lg:w-96"
                  action="#"
                  method="POST"
                >
                  <div className="relative">
                    <input
                      type="email"
                      autoComplete="off"
                      required
                      onChange={(e) => setEmail(e.currentTarget.value)}
                      className="block px-2.5 pl-[1.5rem] pb-2.5 pt-4 w-full text-md text-gray-900 bg-transparent border border-gray-300  outline-none rounded-[100px] bg-white border-1 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    />
                    <label
                      htmlFor="email"
                      className="absolute text-md text-slate-600 duration-300 bg-gray-50 grid place-items-center rounded-[100px] border-none h-[50px] transform -translate-y-3 scale-75 origin-[0] peer-focus:bg-white  px-5  peer-focus:text-blue-600 peer-focus:px-1 peer-focus:h-auto  peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2  peer-focus:top-3 peer-focus:scale-100 peer-focus:-translate-y-6 -top-3 left-1 peer-focus:left-5"
                    >
                      Email
                    </label>
                  </div>

                  <div className="relative">
                    <input
                      type={isShowPassword ? 'text' : 'password'}
                      required
                      autoComplete="off"
                      onChange={(e) => setPassword(e.currentTarget.value)}
                      className="block px-2.5 pl-[1.5rem] pb-2.5 pt-4 w-full text-md text-gray-900 bg-transparent border border-gray-300  outline-none rounded-[100px] bg-white border-1 appearance-none focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                    />
                    <div
                      className="absolute flex justify-end top-3 right-7 text-gray-400"
                      onClick={() => setIsShowPassword((prev) => !prev)}
                    >
                      {isShowPassword ? (
                        <svg
                          stroke="currentColor"
                          fill="currentColor"
                          strokeWidth="0"
                          viewBox="0 0 1024 1024"
                          height="2em"
                          width="2em"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M396 512a112 112 0 1 0 224 0 112 112 0 1 0-224 0zm546.2-25.8C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 0 0 0 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM508 688c-97.2 0-176-78.8-176-176s78.8-176 176-176 176 78.8 176 176-78.8 176-176 176z" />
                        </svg>
                      ) : (
                        <svg
                          stroke="currentColor"
                          fill="currentColor"
                          strokeWidth="0"
                          viewBox="0 0 1024 1024"
                          height="2em"
                          width="2em"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <defs>
                            <clipPath>
                              <path
                                fill="none"
                                d="M124-288l388-672 388 672H124z"
                                clipRule="evenodd"
                              />
                            </clipPath>
                          </defs>
                          <path d="M508 624a112 112 0 0 0 112-112c0-3.28-.15-6.53-.43-9.74L498.26 623.57c3.21.28 6.45.43 9.74.43zm370.72-458.44L836 122.88a8 8 0 0 0-11.31 0L715.37 232.23Q624.91 186 512 186q-288.3 0-430.2 300.3a60.3 60.3 0 0 0 0 51.5q56.7 119.43 136.55 191.45L112.56 835a8 8 0 0 0 0 11.31L155.25 889a8 8 0 0 0 11.31 0l712.16-712.12a8 8 0 0 0 0-11.32zM332 512a176 176 0 0 1 258.88-155.28l-48.62 48.62a112.08 112.08 0 0 0-140.92 140.92l-48.62 48.62A175.09 175.09 0 0 1 332 512z" />
                          <path d="M942.2 486.2Q889.4 375 816.51 304.85L672.37 449A176.08 176.08 0 0 1 445 676.37L322.74 798.63Q407.82 838 512 838q288.3 0 430.2-300.3a60.29 60.29 0 0 0 0-51.5z" />
                        </svg>
                      )}
                    </div>
                    <label
                      htmlFor="password"
                      className="absolute text-md text-slate-600 duration-300 bg-gray-50 grid place-items-center rounded-[100px] border-none h-[50px] transform -translate-y-3 scale-75 origin-[0] peer-focus:bg-white  px-5  peer-focus:text-blue-600 peer-focus:px-1 peer-focus:h-auto  peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2  peer-focus:top-3 peer-focus:scale-100 peer-focus:-translate-y-6 -top-3 left-1 peer-focus:left-5"
                    >
                      Password
                    </label>
                  </div>

                  <div className="flex justify-end">
                    <div className="text-sm">
                      <button
                        type="button"
                        href="#"
                        className="font-medium text-indigo-600 hover:text-indigo-500"
                      >
                        Forgot your password?
                      </button>
                    </div>
                  </div>

                  <div>
                    <button
                      type="submit"
                      className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    >
                      <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                        <svg
                          className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            fillRule="evenodd"
                            d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </span>
                      Sign in
                    </button>

                    <button
                      onClick={googleLogin}
                      type="button"
                      className="group relative flex w-full justify-center rounded-md border-2 border-grey bg-white mt-2 py-2 px-4 text-sm font-medium text-black hover:text-white hover:bg-blue-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    >
                      <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                        <div className="bg-white pt-1 pr-1 pb-1 pl-1 -ml-1">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400"
                            preserveAspectRatio="xMidYMid"
                            viewBox="0 0 256 262"
                          >
                            <path
                              fill="#4285F4"
                              d="M255.878 133.451c0-10.734-.871-18.567-2.756-26.69H130.55v48.448h71.947c-1.45 12.04-9.283 30.172-26.69 42.356l-.244 1.622 38.755 30.023 2.685.268c24.659-22.774 38.875-56.282 38.875-96.027"
                            />
                            <path
                              fill="#34A853"
                              d="M130.55 261.1c35.248 0 64.839-11.605 86.453-31.622l-41.196-31.913c-11.024 7.688-25.82 13.055-45.257 13.055-34.523 0-63.824-22.773-74.269-54.25l-1.531.13-40.298 31.187-.527 1.465C35.393 231.798 79.49 261.1 130.55 261.1"
                            />
                            <path
                              fill="#FBBC05"
                              d="M56.281 156.37c-2.756-8.123-4.351-16.827-4.351-25.82 0-8.994 1.595-17.697 4.206-25.82l-.073-1.73L15.26 71.312l-1.335.635C5.077 89.644 0 109.517 0 130.55s5.077 40.905 13.925 58.602l42.356-32.782"
                            />
                            <path
                              fill="#EB4335"
                              d="M130.55 50.479c24.514 0 41.05 10.589 50.479 19.438l36.844-35.974C195.245 12.91 165.798 0 130.55 0 79.49 0 35.393 29.301 13.925 71.947l42.211 32.783c10.59-31.477 39.891-54.251 74.414-54.251"
                            />
                          </svg>
                        </div>
                      </span>
                      Sign in with Google
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="hover:text-red-300"
        onClick={() => {
          auth.signOut();
        }}
      >
        {user ? `Welcome, ${user.displayName} E-Mail ${user.email}` : ''}
      </div>
    </section>
  );
}

export default Index;
