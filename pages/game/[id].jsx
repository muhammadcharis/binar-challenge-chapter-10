import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import axios from '../api/axios';
import Layout from '../../components/Layout';
import Image from 'next/image';

function DetailPage() {
  const router = useRouter();
  const { id } = router.query;
  const [game, setGame] = useState({});
  useEffect(() => {
    const getDetail = async () => {
      try {
        const response = await axios.get(`games/detail/${id}`);
        setGame(response.data);
      } catch (error) {
        console.log(error, 'error');
      }
    };

    getDetail();
  }, [id]);

  return (
    <Layout>
      <section className="mt-24 mx-auto  w-11/12">
        <div className="grid grid-rows-1 lg:grid-cols-2 gap-4">
          <div className="h-96 rounded-2xl">
            <Image
              src={game.image}
              alt="image1"
              className="object-cover w-full rounded-2xl"
              width={500}
              height={500}
            />
          </div>
          <div className="bg-[#F7FAFC] p-6 rounded-2xl">
            <h1 className="font-extrabold text-2xl lg:text-3xl tracking-wide">
              {game.name}
            </h1>
            <h3 className="font-bold text-lg mt-5">About Game</h3>
            <p className="text-slate-600">{game.description}</p>
            {!game.isPlayable ? (
              <button
                type="button"
                className="px-8 py-3  text-white bg-gray-300 border-none rounded-full w-full mt-24"
                disabled="disabled"
              >
                Under Maintenance
              </button>
            ) : (
              <Link
                href={`/room/${game.id}`}
                className="btn btn-primary bg-[#7189FF] border-none rounded-full w-full mt-24"
              >
                Play
              </Link>
            )}
          </div>
        </div>
      </section>
    </Layout>
  );
}

export default DetailPage;
